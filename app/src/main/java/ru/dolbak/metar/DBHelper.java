package ru.dolbak.metar;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Calendar;


public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "METAR";
    private static final String AIRPORTS_TABLE_NAME = "Airports";

    public DBHelper(Context context){
        super(context,DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s (id INTEGER PRIMARY KEY, icao TEXT, last_use INTEGER, is_favorite INTEGER, airport_name TEXT)", AIRPORTS_TABLE_NAME));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (DATABASE_VERSION < 2){
            db.execSQL(String.format("DROP TABLE IF EXISTS + %s", AIRPORTS_TABLE_NAME));
            onCreate(db);
        }
    }

    public void useAirport(String icao, String airportName){
        SQLiteDatabase db = this.getWritableDatabase();
        if (isNewAirport(db, icao)){
            db.execSQL(String.format("INSERT INTO %s (icao, last_use, is_favorite, airport_name) VALUES(\"%s\", \"%s\", \"%s\", \"%s\")", AIRPORTS_TABLE_NAME, icao, Calendar.getInstance().getTimeInMillis() / 1000L, 0, airportName));
        }
        else{
            db.execSQL(String.format("UPDATE %s SET last_use = \"%s\" WHERE icao = \"%s\"", AIRPORTS_TABLE_NAME, Calendar.getInstance().getTimeInMillis() / 1000L, icao));
        }
    }

    private boolean isNewAirport(SQLiteDatabase db, String icao){
        Cursor res = db.rawQuery(String.format("SELECT COUNT(*) FROM %s WHERE icao=\"%s\"", AIRPORTS_TABLE_NAME, icao), null);
        res.moveToFirst();
        return !(res.getInt(0) > 0);
    }

    public Cursor getAirports(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery(String.format("SELECT icao, airport_name, is_favorite FROM %s ORDER BY last_use DESC LIMIT 10", AIRPORTS_TABLE_NAME), null);
        return cur;
    }
}
