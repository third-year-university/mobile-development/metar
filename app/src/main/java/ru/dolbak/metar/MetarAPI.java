package ru.dolbak.metar;

import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MetarAPI {
    private static String domain = "https://api.checkwx.com";
    private RequestQueue mRequestQueue;
    final String API_KEY = BuildConfig.CHECK_WX_API_KEY;
    public MetarAPI(RequestQueue mRequestQueue) {
        this.mRequestQueue = mRequestQueue;
    }

    public void getMetar(String icao, final VolleyCallBack callback){
        String urlSuffix = "/metar/" + icao + "/decoded";
        String url = domain + urlSuffix;

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, //GET - API-запрос для получение данных
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data"); //получаем JSON-обьекты main и wind (в фигурных скобках - объекты, в квадратных - массивы (JSONArray).
                    JSONObject myData = null;
                    if (data.length() > 0){
                        myData = data.getJSONObject(0);
                    }
                    Metar metar = new Metar(myData);
                    Log.d("METAR", response.toString());
                    callback.onSuccess(metar);
                } catch (Exception e) {
                    callback.onError();
                    Log.d("METAR", "JSON EXCEPTION ");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("METAR", "OTHER EXCEPTION ");
                error.printStackTrace();
            } // в случае возникновеня ошибки
        }) {    //this is the part, that adds the header to the request
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("X-API-Key", API_KEY);
                return params;
            }
        };

        mRequestQueue.add(request); // добавляем запрос в очередь


    }
}

