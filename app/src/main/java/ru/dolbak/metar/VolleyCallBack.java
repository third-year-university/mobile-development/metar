package ru.dolbak.metar;

public interface VolleyCallBack {
    void onSuccess(Metar metar);
    void onError();
}
