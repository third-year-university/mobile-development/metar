package ru.dolbak.metar;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AirportAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> icaos;
    private final ArrayList<String> names;

    public AirportAdapter(Activity context, ArrayList<String> icaos, ArrayList<String> names) {
        super(context, R.layout.airport_list_layout, icaos);

        this.context = context;
        this.icaos = icaos;
        this.names = names;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.airport_list_layout, null,true);

        TextView numbers = rowView.findViewById(R.id.number_list);
        TextView icaoText = rowView.findViewById(R.id.icao_list);
        TextView nameText = rowView.findViewById(R.id.name_list);

        numbers.setText(Integer.toString(position + 1));
        icaoText.setText(icaos.get(position));
        nameText.setText(names.get(position));

        return rowView;
    }
}
