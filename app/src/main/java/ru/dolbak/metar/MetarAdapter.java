package ru.dolbak.metar;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MetarAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> headers;
    private final ArrayList<String> values;

    public MetarAdapter(Activity context, ArrayList<String> headers, ArrayList<String> values) {
        super(context, R.layout.list_layout, headers);

        this.context = context;
        this.headers = headers;
        this.values = values;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_layout, null,true);

        TextView headerText = rowView.findViewById(R.id.list_header);
        TextView valueText = rowView.findViewById(R.id.list_value);

        headerText.setText(headers.get(position));
        valueText.setText(values.get(position));

        return rowView;
    }
}
