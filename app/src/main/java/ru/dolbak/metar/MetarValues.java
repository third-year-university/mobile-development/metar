package ru.dolbak.metar;

import static java.util.Map.entry;

import java.util.Map;

public class MetarValues {
    static private Map<String, String> conditionsRus = Map.ofEntries(
            entry("VC", "Вблизи"),
            entry("PRFG", "Аэродром частично покрыт туманом"),
            entry("VA", "Вулканический пепел"),
            entry("GR", "Град"),
            entry("TS", "Гроза"),
            entry("TSGR", "Гроза с градом"),
            entry("TSRA", "Гроза с дождём"),
            entry("TSDS", "Гроза с пыльной бурей"),
            entry("TSSN", "Гроза со снегом"),
            entry("RA", "Дождь"),
            entry("RASN", "Дождь со снегом"),
            entry("FU", "Дым"),
            entry("BR", "Дымка"),
            entry("PE", "Ледяной дождь"),
            entry("IC", "Ледяные иглы"),
            entry("SH", "Ливень"),
            entry("SHPE", "Ливневый ледяной дождь"),
            entry("SHRASN", "Ливневый дождь со снегом"),
            entry("SHSN", "Ливневый снег"),
            entry("SHSNRA", "Ливневый снег с дождём"),
            entry("HZ", "Мгла"),
            entry("DZ", "Морось"),
            entry("FZRA", "Гололёд, Переохлаждённый дождь"),
            entry("FZDZ", "Гололёд, Переохлаждённая морость"),
            entry("FZFG", "Гололёд, Переохлаждённый туман"),
            entry("SA", "Песок"),
            entry("SS", "Песчаная буря"),
            entry("BLSA", "Песчаная низовая буря"),
            entry("DRSA", "Песчаный поземок"),
            entry("MIFG", "Поземный туман"),
            entry("DR", "Поземок"),
            entry("DU", "Пыль"),
            entry("DS", "Пыльная буря"),
            entry("BLDU", "Пыльная низовая метель"),
            entry("PO", "Пыльные вихри"),
            entry("DRDU", "Пыльный поземок"),
            entry("FC", "Смерч"),
            entry("SN", "Снег"),
            entry("SNRA", "Снег с дождём"),
            entry("GS", "Снежная крупа"),
            entry("BLSN", "Снежная низовая метель"),
            entry("SG", "Снежные зёрна"),
            entry("FG", "Туман"),
            entry("BCFG", "Туман местами"),
            entry("SQ", "Шквал")
    );

    static private Map<String, String> cloudsRus = Map.ofEntries(
            entry("FEW", "Незначительная"),
            entry("SCT", "Рассеянная"),
            entry("BKN", "Значительная"),
            entry("OVC", "Сплошная"),
            entry("SKC", "Ясно"),
            entry("CAVOK", "Ясно"),
            entry("CLR", "Ясно"),
            entry("NSC", "Без существенной облачности"),
            entry("CB", "Кучево-дождевая"),
            entry("TCU", "Мощно-кучевая"),
            entry("VV", "Вертикальная видимость"),
            entry("OVX", "Вертикальная видимость"),
            entry("STS", "Отдельные облака"),
            entry("NSW", "прекращение ясной погоды"),
            entry("ACC", "Высоко-кучевая башенковидная")
            );

    static private Map<String, String> numbers = Map.ofEntries(
            entry("1", "one"),
            entry("2", "two"),
            entry("3", "tree"),
            entry("4", "four"),
            entry("5", "five"),
            entry("6", "six"),
            entry("7", "seven"),
            entry("8", "eight"),
            entry("9", "niner"),
            entry("0", "zero")
    );

    static private Map<String, String> numbersRus = Map.ofEntries(
            entry("-", "минус"),
            entry("1", "один"),
            entry("2", "два"),
            entry("3", "три"),
            entry("4", "четыре"),
            entry("5", "пять"),
            entry("6", "шесть"),
            entry("7", "семь"),
            entry("8", "восемь"),
            entry("9", "девять"),
            entry("0", "")
    );

    static private Map<String, String> decNumbersRus = Map.ofEntries(
            entry("1", "десять"),
            entry("2", "двадцать"),
            entry("3", "тридцать"),
            entry("4", "сорок"),
            entry("5", "пятьдесят"),
            entry("6", "шестьдесят"),
            entry("7", "семьдесят"),
            entry("8", "восемьдесять"),
            entry("9", "девяносто"),
            entry("0", "")
    );

    static private Map<String, String> dec1NumbersRus = Map.ofEntries(
            entry("10", "десять"),
            entry("11", "одиннадцать"),
            entry("12", "двенадцать"),
            entry("13", "тринадцать"),
            entry("14", "четырнадцать"),
            entry("15", "пятнадцать"),
            entry("16", "шестнадцать"),
            entry("17", "семнадцать"),
            entry("18", "восемнадцать"),
            entry("19", "девятнадцать")
    );

    static private Map<String, String> hundredsRus = Map.ofEntries(
            entry("1", "сто"),
            entry("2", "двести"),
            entry("3", "триста"),
            entry("4", "четыреста"),
            entry("5", "пятсот"),
            entry("6", "шестсот"),
            entry("7", "семьсот"),
            entry("8", "восемсот"),
            entry("9", "девятсот"),
            entry("0", "")
    );

    static private Map<String, String> thousandsRus = Map.ofEntries(
            entry("1", "тысяча"),
            entry("2", "две тысячи"),
            entry("3", "три тысячи"),
            entry("4", "четыре тысячи"),
            entry("5", "пять тысяч"),
            entry("6", "шесть тысяч"),
            entry("7", "семь тысяч"),
            entry("8", "восемь тысяч"),
            entry("9", "девять тысяч"),
            entry("0", "")
    );

    static private Map<String, String> alphabet = Map.ofEntries(
            entry("A", "ALPHA"),
            entry("B", "BRAVO"),
            entry("C", "CHARLIE"),
            entry("D", "DELTA"),
            entry("E", "ECHO"),
            entry("F", "FOXTROT"),
            entry("G", "GOLF"),
            entry("H", "HOTEL"),
            entry("I", "INDIA"),
            entry("J", "JULIET"),
            entry("K", "KILO"),
            entry("L", "LIMA"),
            entry("M", "MIKE"),
            entry("N", "NOVEMBER"),
            entry("O", "OSCAR"),
            entry("P", "PAPA"),
            entry("Q", "QUEBEC"),
            entry("R", "ROMEO"),
            entry("S", "SIERRA"),
            entry("T", "TANGO"),
            entry("U", "UNIFORM"),
            entry("V", "VICTOR"),
            entry("W", "WHISKY"),
            entry("X", "X-RAY"),
            entry("Y", "YANKEE"),
            entry("Z", "ZULU")
    );

    static private Map<String, String> alphabetRus = Map.ofEntries(
            entry("A", "АЛЬФА"),
            entry("B", "БРАВО"),
            entry("C", "ЧАРЛИ"),
            entry("D", "ДЭЛЬТА"),
            entry("E", "ЭКО"),
            entry("F", "ФОКСТРОТ"),
            entry("G", "ГОЛЬФ"),
            entry("H", "ХОТЭЛ"),
            entry("I", "ИНДИА"),
            entry("J", "ДЖУЛЬЕТ"),
            entry("K", "КИЛО"),
            entry("L", "ЛИМА"),
            entry("M", "МАЙК"),
            entry("N", "НОВЭМБЕР"),
            entry("O", "ОСКАР"),
            entry("P", "ПАПА"),
            entry("Q", "КЕБЕК"),
            entry("R", "РОМЕО"),
            entry("S", "СЬЕРА"),
            entry("T", "ТАНГО"),
            entry("U", "ЮНИФОРМ"),
            entry("V", "ВИКТОР"),
            entry("W", "ВИСКИ"),
            entry("X", "ЭКСРЭЙ"),
            entry("Y", "ЯНКИ"),
            entry("Z", "ЗУЛУ")
    );


    public static String pronounceLetters(String letters, String lang){
        StringBuilder answer = new StringBuilder();
        for (int i = 0; i < letters.length(); i++){
            String letter = Character.toString(letters.charAt(i));
            if (alphabet.containsKey(letter)){
                switch (lang){
                    case "ENG":
                        answer.append(alphabet.get(letter)).append(" ");
                        break;
                    case "RUS":
                        answer.append(alphabetRus.get(letter)).append(" ");
                        break;
                }
            }
        }
        return answer.toString();
    }

    public static String pronounceNumbers(String letters, String lang){
        StringBuilder answer = new StringBuilder();
        for (int i = 0; i < letters.length(); i++){
            String letter = Character.toString(letters.charAt(i));
            if (numbers.containsKey(letter)){
                switch (lang){
                    case "ENG":
                        answer.append(numbers.get(letter)).append(", ");
                        break;
                    case "RUS":
                        if (letter.equals("0")){
                            answer.append("ноль").append(", ");
                            continue;
                        }
                        answer.append(numbersRus.get(letter)).append(", ");
                        break;
                }
            }
        }
        return answer.toString();
    }

    public static String pronounceNumbersComplexRus(int number){
        if (number == 0){
            return "ноль";
        }
        String answer = "";
        boolean minus = false;
        if (number < 0){
            minus = true;
            number = -number;
        }
        if (number % 100 >= 10 && number % 100 <= 19){
            answer = dec1NumbersRus.get(Integer.toString(number % 100)) + ", " + answer;
        }
        else{
            answer = decNumbersRus.get(Integer.toString(number / 10 % 10)) + ", " + numbersRus.get(Integer.toString(number % 10)) + ", " + answer;
        }
        answer = hundredsRus.get(Integer.toString(number / 100 % 10)) + ", " + answer;
        answer = thousandsRus.get(Integer.toString(number / 1000 % 10)) + ", " + answer;

        if (minus){
            answer = "минус " + answer;
        }

        return answer;
    }

    public static String conditionCodeToTextRussian(String code, String prefix){
        String answer = "";
        if (conditionsRus.containsKey(code)){
            answer =  conditionsRus.get(code);
        }
        if (prefix != null && prefix.equals("-")){
            answer = "слабый " + answer;
        }
        if (prefix != null && prefix.equals("+")){
            answer = "значительный " + answer;
        }
        return answer;
    }

    public static String cloudsCodeToTextRussian(String code){
        if (cloudsRus.containsKey(code)){
            return cloudsRus.get(code);
        }
        return "";
    }
}
