package ru.dolbak.metar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    Button buttonFind;
    EditText editICAO;
    RequestQueue mRequestQueue;
    MetarAPI metarAPI;
    DBHelper dbHelper;
    ListView listView;

    ArrayList<String> icaos;
    ArrayList<String> names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonFind = findViewById(R.id.buttonFind);
        editICAO = findViewById(R.id.editICAO);
        listView = findViewById(R.id.airports_list);
        mRequestQueue = Volley.newRequestQueue(this);
        dbHelper = new DBHelper(this);

        metarAPI = new MetarAPI(mRequestQueue);

        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                switchToAirport(icaos.get(position));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillListView();
        editICAO.setText("");
    }

    public void getMetar(View view) {
        switchToAirport(editICAO.getText().toString().toLowerCase());
    }

    public void switchToAirport(String icao){
        try{
            metarAPI.getMetar(icao, new VolleyCallBack() {
                @Override
                public void onSuccess(Metar metar) {
                    dbHelper.useAirport(icao, metar.station.name);
                    Intent intent = new Intent(getApplicationContext(), Airport.class);
                    intent.putExtra("METAR", metar);
                    startActivity(intent);
                }
                public void onError() {
                    Toast.makeText(getApplicationContext(), "Аэродром не найден", Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e){
            Toast.makeText(this, "Произошла ошибка", Toast.LENGTH_SHORT).show();
        }
    }

    public void fillListView(){
        icaos = new ArrayList<>();
        names = new ArrayList<>();
        Cursor cur = dbHelper.getAirports();
        cur.moveToFirst();
        Log.d("DBASE", "GOOOOOOOOOOOO");
        while (!cur.isAfterLast()){
            icaos.add(cur.getString(0).toUpperCase());
            names.add(cur.getString(1));
            Log.d("DBASE", cur.getString(0) + " " + cur.getString(1));
            cur.moveToNext();
        }
        AirportAdapter adapter = new AirportAdapter(this, icaos, names);
        listView.setAdapter(adapter);
    }
}