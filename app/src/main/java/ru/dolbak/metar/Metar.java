package ru.dolbak.metar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Metar implements Serializable {
    String flight_category = null;
    String icao = null;
    String observed = null;
    String raw_text = null;
    Barometer barometer = null;
    ArrayList<Cloud> clouds = null;
    Dewpoint dewpoint = null;
    Elevation elevation = null;
    Humidity humidity = null;
    Station station = null;
    Temperature temperature = null;
    Visibility visibility = null;
    Wind wind = null;
    Ceiling ceiling = null;
    ArrayList<Condition> conditions = null;
    Rain rain = null;
    Snow snow = null;
    Windchill windchill = null;

    public Metar(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("flight_category")){
            flight_category = jsonObject.getString("flight_category");
        }
        if (jsonObject.has("icao")){
            icao = jsonObject.getString("icao");
        }
        if (jsonObject.has("observed")){
            observed = jsonObject.getString("observed");
        }
        if (jsonObject.has("raw_text")){
            raw_text = jsonObject.getString("raw_text");
        }
        if (jsonObject.has("barometer")){
            barometer = new Barometer(jsonObject.getJSONObject("barometer"), raw_text);
        }
        if (jsonObject.has("dewpoint")){
            dewpoint = new Dewpoint(jsonObject.getJSONObject("dewpoint"));
        }
        if (jsonObject.has("elevation")){
            elevation = new Elevation(jsonObject.getJSONObject("elevation"));
        }
        if (jsonObject.has("humidity")){
            humidity = new Humidity(jsonObject.getJSONObject("humidity"));
        }
        if (jsonObject.has("station")){
            station = new Station(jsonObject.getJSONObject("station"));
        }
        if (jsonObject.has("temperature")){
            temperature = new Temperature(jsonObject.getJSONObject("temperature"));
        }
        if (jsonObject.has("visibility")){
            visibility = new Visibility(jsonObject.getJSONObject("visibility"));
        }
        if (jsonObject.has("wind")){
            wind = new Wind(jsonObject.getJSONObject("wind"));
        }
        if (jsonObject.has("clouds")){
            clouds = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("clouds");
            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                clouds.add(new Cloud(jsonObject1));
            }
        }
        if (jsonObject.has("ceiling")){
            ceiling = new Ceiling(jsonObject.getJSONObject("ceiling"));
        }
        if (jsonObject.has("conditions")){
            conditions = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("conditions");
            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                conditions.add(new Condition(jsonObject1));
            }
        }
        if (jsonObject.has("rain")){
            rain = new Rain(jsonObject.getJSONObject("rain"));
        }
        if (jsonObject.has("snow")){
            snow = new Snow(jsonObject.getJSONObject("snow"));
        }
        if (jsonObject.has("windchill")){
            windchill = new Windchill(jsonObject.getJSONObject("windchill"));
        }

    }

    public String getAtisTextRu(){
        String answer = "";
        String lang = "RUS";
        int currentTime = Calendar.getInstance().getTime().getHours();
        String litera = Character.toString((char) ('A' + currentTime));
        String information_code = MetarValues.pronounceLetters(litera, lang).toLowerCase(new Locale("ru", "RU"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        Date date = null;
        try {
            date = dateFormat.parse(observed);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String hours = MetarValues.pronounceNumbersComplexRus(date.getHours());
        String minutes = MetarValues.pronounceNumbersComplexRus(date.getMinutes());

        answer = station.name + " АТИС. Информация: " + information_code + ". ";
        if (date.getHours() < 10){
            answer += "ноль ";
        }
        answer += hours + " ";
        if (date.getMinutes() < 10){
            answer += "ноль ";
        }
        answer += minutes + ". "
                + "Ветер у земли: " + MetarValues.pronounceNumbersComplexRus(wind.degrees) +
                " градусов, " + MetarValues.pronounceNumbersComplexRus(wind.speed_mps) +
                " метров в секунду. ";
        if (conditions != null){
            for (Condition condition : conditions){
                answer += condition.russianText + ", ";
            }
            answer += ". ";
        }
        if (clouds != null){
            answer += " Облачность: ";
            for (Cloud cloud : clouds){
                answer += cloud.russianText +  " " + MetarValues.pronounceNumbersComplexRus(cloud.meters) + " метров, ";
            }
            answer += ". ";
        }

        answer += "Температура: " + MetarValues.pronounceNumbersComplexRus(temperature.celsius) +
                ", точка росы: " + MetarValues.pronounceNumbersComplexRus(dewpoint.celsius) + ". Кью эн эйч: " +
        MetarValues.pronounceNumbers(String.valueOf((int) barometer.hpa), lang) + " гектопаскалей. ";
        if (barometer.qfe_hg != 0.0 && barometer.qfe_hpa != 0.0){
            answer += "Кью эф ии: " + MetarValues.pronounceNumbers(String.valueOf((int) barometer.qfe_hg), lang) +
                    "милиметров или: " +  MetarValues.pronounceNumbers(String.valueOf((int) barometer.qfe_hpa), lang)  +
                    "гектопаскалей. ";
        }

        answer += "Сообщите получение: " + information_code;

        return answer;
    }

    public String getAtisTextEn(){
        String answer = "";
        String lang = "ENG";
        int currentTime = Calendar.getInstance().getTime().getHours();
        String litera = Character.toString((char) ('A' + currentTime));
        String information_code = MetarValues.pronounceLetters(litera, lang).toLowerCase(new Locale("ru", "RU"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        Date date = null;
        try {
            date = dateFormat.parse(observed);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String hours = MetarValues.pronounceNumbers(String.valueOf(date.getHours()), lang);
        String minutes = MetarValues.pronounceNumbers(String.valueOf(date.getMinutes()), lang);

        answer += station.name + " ATIS. Information " + information_code + ". ";
        answer += hours + " ";
        answer += minutes + ". ";
        answer += "On final wind is " + MetarValues.pronounceNumbers(String.valueOf(wind.degrees), lang) +
                " degrees, " + MetarValues.pronounceNumbers(String.valueOf(wind.speed_mps), lang) + " metres per second. ";

        if (conditions != null){
            for (Condition condition : conditions){
                answer += condition.text + ", ";
            }
            answer += ". ";
        }
        if (clouds != null){
            answer += " Clouds: ";
            for (Cloud cloud : clouds){
                answer += cloud.text +  " " + MetarValues.pronounceNumbers(String.valueOf(cloud.meters), lang) + " metres, ";
            }
            answer += ". ";
        }

        answer += "Temperature: " + MetarValues.pronounceNumbers(String.valueOf(temperature.celsius), lang) +
                ", dew point: " + MetarValues.pronounceNumbers(String.valueOf(dewpoint.celsius), lang);
        answer += ". QNH: " + MetarValues.pronounceNumbers(String.valueOf((int) barometer.hpa), lang)
                + "hectopascals. ";

        if (barometer.qfe_hg != 0.0 && barometer.qfe_hpa != 0.0){
            answer += "QFE: " + MetarValues.pronounceNumbers(String.valueOf((int) barometer.qfe_hg), lang) +
                    "millimeters or: " +  MetarValues.pronounceNumbers(String.valueOf((int) barometer.qfe_hpa), lang)  +
                    "hectopascals. ";
        }

        answer += "Acknowledge information: " + information_code;
        return answer;
    }
}



class Barometer implements Serializable{
    double hg;
    double hpa;
    double kpa;
    double mb;
    double qfe_hpa;
    double qfe_hg;

    public Barometer(JSONObject jsonObject, String raw_text) throws JSONException {
        if (jsonObject.has("hg")){
            hg = jsonObject.getDouble("hg");
        }
        if (jsonObject.has("hpa")){
            hpa = jsonObject.getDouble("hpa");
        }
        if (jsonObject.has("kpa")){
            kpa = jsonObject.getDouble("kpa");
        }
        if (jsonObject.has("mb")){
            mb = jsonObject.getDouble("mb");
        }
        if (raw_text != null){
            int qfe_index = raw_text.indexOf("QFE");
            if (qfe_index >= 0){
                String[] values = raw_text.substring(qfe_index + 3, qfe_index + 11).split("/");
                qfe_hg = Double.parseDouble(values[0]);
                qfe_hpa = Double.parseDouble(values[1]);
            }
        }
    }
}

class Ceiling implements Serializable {
    int base_feet_agl;
    int base_meters_agl;
    String code = null;
    int feet;
    int meters;
    String text = null;
    String russianText = null;

    public Ceiling(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("base_feet_agl")){
            base_feet_agl = jsonObject.getInt("base_feet_agl");
        }
        if (jsonObject.has("base_meters_agl")){
            base_meters_agl = jsonObject.getInt("base_meters_agl");
        }
        if (jsonObject.has("feet")){
            feet = jsonObject.getInt("feet");
        }
        if (jsonObject.has("meters")){
            meters = jsonObject.getInt("meters");
        }
        if (jsonObject.has("code")){
            code = jsonObject.getString("code");
            russianText = MetarValues.cloudsCodeToTextRussian(code);
        }
        if (jsonObject.has("text")){
            text = jsonObject.getString("text");
        }
    }
}

class Cloud implements Serializable {
    int base_feet_agl;
    int base_meters_agl;
    String code = null;
    int feet;
    int meters;
    String text = null;
    String russianText = null;

    public Cloud(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("base_feet_agl")){
            base_feet_agl = jsonObject.getInt("base_feet_agl");
        }
        if (jsonObject.has("base_meters_agl")){
            base_meters_agl = jsonObject.getInt("base_meters_agl");
        }
        if (jsonObject.has("feet")){
            feet = jsonObject.getInt("feet");
        }
        if (jsonObject.has("meters")){
            meters = jsonObject.getInt("meters");
        }
        if (jsonObject.has("code")){
            code = jsonObject.getString("code");
            russianText = MetarValues.cloudsCodeToTextRussian(code);
        }
        if (jsonObject.has("text")){
            text = jsonObject.getString("text");
        }
    }
}

class Dewpoint implements Serializable {
    int celsius;
    int fahrenheit;

    public Dewpoint(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("celsius")){
            celsius = jsonObject.getInt("celsius");
        }
        if (jsonObject.has("fahrenheit")){
            fahrenheit = jsonObject.getInt("fahrenheit");
        }
    }
}

class Elevation implements Serializable {
    int feet;
    int meters;

    public Elevation(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("feet")){
            feet = jsonObject.getInt("feet");
        }
        if (jsonObject.has("meters")){
            meters = jsonObject.getInt("meters");
        }

    }
}

class Humidity implements Serializable {
    int percent;

    public Humidity(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("percent")){
            percent = jsonObject.getInt("percent");
        }
    }
}

class Station implements Serializable{
    String location = null;
    String name = null;
    String type = null;
    Geometry geometry = null;

    public Station(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("location")){
            location = jsonObject.getString("location");
        }
        if (jsonObject.has("name")){
            name = jsonObject.getString("name");
        }
        if (jsonObject.has("type")){
            type = jsonObject.getString("type");
        }
        if (jsonObject.has("geometry")){
            geometry = new Geometry(jsonObject.getJSONObject("geometry"));
        }

    }
}

class Geometry implements Serializable{
    ArrayList<Double> coordinates = null;
    String type = null;

    public Geometry(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("type")){
            type = jsonObject.getString("type");
        }
        if (jsonObject.has("coordinates")){
            coordinates = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("coordinates");
            for (int i = 0; i < jsonArray.length(); i++){
                coordinates.add(jsonArray.getDouble(i));
            }
        }
    }
}

class Temperature implements Serializable{
    int celsius;
    int fahrenheit;

    public Temperature(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("celsius")){
            celsius = jsonObject.getInt("celsius");
        }
        if (jsonObject.has("fahrenheit")){
            fahrenheit = jsonObject.getInt("fahrenheit");
        }
    }
}

class Visibility implements Serializable{
    String meters = null;
    double meters_float;
    String miles = null;
    double miles_float;

    public Visibility(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("meters")){
            meters = jsonObject.getString("meters");
        }
        if (jsonObject.has("meters_float")){
            meters_float = jsonObject.getDouble("meters_float");
        }
        if (jsonObject.has("miles")){
            miles = jsonObject.getString("miles");
        }
        if (jsonObject.has("miles_float")){
            miles_float = jsonObject.getDouble("miles_float");
        }

    }
}

class Wind implements Serializable{
    int degrees;
    int speed_kph;
    int speed_kts;
    int speed_mph;
    int speed_mps;
    int gust_kts;
    int gust_kph;
    int gust_mph;
    int gust_mps;

    public Wind(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("degrees")){
            degrees = jsonObject.getInt("degrees");
        }
        if (jsonObject.has("speed_kph")){
            speed_kph = jsonObject.getInt("speed_kph");
        }
        if (jsonObject.has("speed_kts")){
            speed_kts = jsonObject.getInt("speed_kts");
        }
        if (jsonObject.has("speed_mph")){
            speed_mph = jsonObject.getInt("speed_mph");
        }
        if (jsonObject.has("speed_mps")){
            speed_mps = jsonObject.getInt("speed_mps");
        }
        if (jsonObject.has("gust_kts")){
            gust_kts = jsonObject.getInt("gust_kts");
        }
        if (jsonObject.has("gust_kph")){
            gust_kph = jsonObject.getInt("gust_kph");
        }
        if (jsonObject.has("gust_mph")){
            gust_mph = jsonObject.getInt("gust_mph");
        }
        if (jsonObject.has("gust_mps")){
            gust_mps = jsonObject.getInt("gust_mps");
        }
    }
}

class Windchill implements Serializable{
    int celsius;
    int fahrenheit;

    public Windchill(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("celsius")){
            celsius = jsonObject.getInt("celsius");
        }
        if (jsonObject.has("fahrenheit")){
            fahrenheit = jsonObject.getInt("fahrenheit");
        }
    }
}

class Condition implements Serializable{
    String code = null;
    String text = null;
    String prefix = null;
    String russianText = null;

    public Condition(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("prefix")){
            prefix = jsonObject.getString("prefix");
        }
        if (jsonObject.has("code")){
            code = jsonObject.getString("code");
            russianText = MetarValues.conditionCodeToTextRussian(code, prefix);
        }
        if (jsonObject.has("text")){
            text = jsonObject.getString("text");
        }
    }
}

class Rain implements Serializable{
    int inches;

    public Rain(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("inches")){
            inches = jsonObject.getInt("inches");
        }
    }
}

class Snow implements Serializable{
    int inches;

    public Snow(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("inches")){
            inches = jsonObject.getInt("inches");
        }
    }
}
