package ru.dolbak.metar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Airport extends AppCompatActivity {

    Metar metar;
    TextToSpeech textToSpeechRu, textToSpeechEn;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airport);


        Intent mIntent = getIntent();

        metar = (Metar) mIntent.getSerializableExtra("METAR");

        textToSpeechRu = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    textToSpeechRu.setLanguage(new Locale("ru", "RU"));
                }
            }
        });

        textToSpeechEn = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    textToSpeechEn.setLanguage(Locale.US);
                }
            }
        });

        listView = findViewById(R.id.listView);

        generateList(metar);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        textToSpeechRu.stop();
        textToSpeechEn.stop();
    }

    private void generateList(Metar metar){
        ArrayList<String> headers = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();
        if (metar.station.name != null){
            headers.add("Аэропорт");
            values.add(metar.station.name);
        }
        if (metar.elevation != null){
            headers.add("Превышение");
            values.add(metar.elevation.feet + " ф / " + metar.elevation.meters + " м");
        }
        if (metar.observed != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm");
            Date date = null;
            try {
                date = dateFormat.parse(metar.observed);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            headers.add("Время");
            values.add(dateFormat2.format(date));
        }
        if (metar.barometer != null){
            headers.add("QNH");
            values.add((int) metar.barometer.hpa + " гПа / " + metar.barometer.hg + " д рт ст");
            if (metar.barometer.qfe_hpa > 0 && metar.barometer.qfe_hg > 0){
                headers.add("QFE");
                values.add((int) metar.barometer.qfe_hg + " мм рт ст / " + (int) metar.barometer.qfe_hpa + " гПа");
            }
        }
        if (metar.wind != null){
            headers.add("Ветер");
            values.add(metar.wind.degrees + "° " + metar.wind.speed_mps + " м/c");
        }
        if (metar.temperature != null){
            headers.add("Температура");
            values.add(metar.temperature.celsius + "°C");
        }
        if (metar.dewpoint != null){
            headers.add("Точка росы");
            values.add(metar.dewpoint.celsius + "°C");
        }
        if (metar.humidity != null){
            headers.add("Влажность");
            values.add(metar.humidity.percent + "%");
        }
        if (metar.visibility != null){
            headers.add("Видимость");
            values.add(metar.visibility.meters + " м");
        }
        if (metar.conditions != null && metar.conditions.size() > 0){
            headers.add("Погодные явления");
            String cond = "";
            for (Condition condition : metar.conditions){
                cond += condition.russianText + ", ";
            }
            values.add(cond);
        }
        if (metar.clouds != null && metar.clouds.size() > 0){
            headers.add("Облачность");
            String cl = "";
            for (Cloud cloud : metar.clouds){
                cl += cloud.russianText + " " + cloud.meters + " м" + ", ";
            }
            values.add(cl);
        }
        MetarAdapter adapterV = new MetarAdapter(this, headers, values);

        listView.setAdapter(adapterV);

    }

    public void playAtis(View view) {
        String text = metar.getAtisTextRu();
        Log.d("ATIS", text);
        textToSpeechRu.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void playAtisEN(View view) {
        String text = metar.getAtisTextEn();
        Log.d("ATIS", text);
        textToSpeechEn.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    public void stopAtis(View view) {
        textToSpeechEn.stop();
        textToSpeechRu.stop();
    }
}